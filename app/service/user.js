'use strict';

const { Service } = require("egg");
const { mysql } = require("../../config/plugin");

class User extends Service {
    async create ({sex}) {
        const {ctx, app} = this;
        const result = await app.mysql.insert('rent_user', {
            nick_name: '唐果果',
            sex: 2
        })
        console.log(result);

        return result;
    }

    async update (user) {
        const {ctx, app} = this;
        console.log(user)
        const result = await app.mysql.update('rent_user', ctx.request.body);
        return result;
    }

    
    
}

module.exports = User