'use strict';

const Controller = require('egg').Controller;

class User extends Controller {
    async index () {
        const {ctx, service} = this;
        ctx.body = await service.user.create({sex: '男'})
    }

    async getUsers () {
        const {ctx, app} = this;
        const users = await app.mysql.query('SELECT * FROM rent_user');
        ctx.body = users;
    }

    async getUserById () {
        const {ctx, service} = this;

    }
    /**
     * @Description 
     * @author 孙畅
     * @date 2020-11-29
     * @returns {any}
     */
    async updateUser () {
        const {ctx, service} = this;
        ctx.body = await service.user.update(ctx.request.body);
        
    }
}

module.exports = User